@echo off

IF [%1]==[] (
    echo Require Project Name
    GOTO:EOF
) else (set PROJECTNAME=%1)

SET BUILDING=false
SET TOS=windows-x64

if [%2]==[build] (
  SET BUILDING=true
  IF [%3]==[windows-x64] (set TOS=%3)
  IF [%3]==[linux-x64] (set TOS=%3)
  IF [%3]==[mac-x64] (set TOS=%3)
  shift
)

SET UPLOAD=false
SET UPLOADTARGET=all
if [%2]==[upload] (
  SET UPLOAD=true
  IF NOT [%3]==[] (set UPLOADTARGET=%3)
  shift
)

:: Set some default values
SET PROJECTSROOTS=C:
SET UNITYHUBROOT=C:
SET TMPDIR=.temps
SET CONFIGFILENAME=config.cfg
SET KEYFILENAME=privatekey


call:LoadConfigTools
SET PROJECTPATH=%PROJECTSROOTS%\%PROJECTNAME%
SET UNITYPROJECTPATH=%PROJECTPATH%\%PROJECTNAME%-Unity
SET CONFIGDIR=%PROJECTPATH%\BuildToolsConfigs



SET VERSION=0.0.0.0
SET COMPANYNAME=Lorem
SET PRODUCTNAME=Ipsum
SET UNITYVERSION=0.0.0.f
SET DEMOTAG=""

call:LoadUnityInfos

call:CheckInstall

SET BUILDLOCATION=%~dp0Builds\%PRODUCTNAME%
SET BUILDLOCATIONFINALDIR=%BUILDLOCATION%\%VERSION%

if [%BUILDING%]==[true] call:BuildFunc
if [%UPLOAD%]==[true] (
  if [%UPLOADTARGET%]==[all] (
    ::Upload for all
    for %%f in (Modules\*.cmd) do (
      echo --------------------------------------------
      call %%f Upload
      echo --------------------------------------------
    )
  ) else (
    call Modules\%UPLOADTARGET%.cmd Upload
  )
)

echo Job Finish !
echo Bye :)
GOTO:EOF

:: START OF FUNCTIONS

:LoadConfigTools
echo Reading infos from tool config
for /f "tokens=1,2 delims==" %%a in (%CONFIGFILENAME%) DO (
  if "%%a" == "projectroots" SET PROJECTSROOTS=%%b
  if "%%a" == "unityhubroot" SET UNITYHUBROOT=%%b
)
GOTO:EOF

:LoadUnityInfos
echo Reading infos from unity
for /f "tokens=1,2 delims=:" %%a in (%UNITYPROJECTPATH%\ProjectSettings\ProjectSettings.asset) DO (
  if "%%a" == "  bundleVersion" SET VERSION=%%b
  if "%%a" == "  companyName" SET COMPANYNAME=%%b
  if "%%a" == "  productName" SET PRODUCTNAME=%%b
)

for /f "tokens=1,2 delims=:" %%a in (%UNITYPROJECTPATH%\ProjectSettings\ProjectVersion.txt) DO (
  if "%%a" == "m_EditorVersion" SET UNITYVERSION=%%b
)

:: Cleaning var
SET VERSION=%VERSION:~1%
SET COMPANYNAME=%COMPANYNAME:~1%
SET PRODUCTNAME=%PRODUCTNAME:~1%
SET UNITYVERSION=%UNITYVERSION:~1%

echo Project %PRODUCTNAME% version %VERSION% for %COMPANYNAME% with Unity %UNITYVERSION%
if "%VERSION:~-1%" == "d" (
  SET DEMOTAG=-DEMO
  echo Version fishing by "D" it's a demo version
)

GOTO:EOF

:CheckInstall
echo Checking instalation
if not exist %CONFIGDIR% mkdir %CONFIGDIR%
if exist %TMPDIR% (
  rmdir /S /Q "%TMPDIR%"
  mkdir "%TMPDIR%"
)
echo --------------------------------------------

::update framwork dependents config
for %%f in (Modules\*.cmd) do (
  call %%f CheckConfig
  echo --------------------------------------------
)

GOTO:EOF

:BuildFunc    - Function for building your unity project

if %TOS% == windows-x64 SET BUILDTARGET=StandaloneWindows64
if %TOS% == linux-x64 SET BUILDTARGET=StandaloneLinux64
if %TOS% == mac-x64 SET BUILDTARGET=StandaloneOSX

echo Building for %BUILDTARGET% with %UNITYVERSION%

echo Preparing %BUILDLOCATIONFINALDIR%
rmdir /S /Q "%BUILDLOCATION%"
mkdir "%BUILDLOCATIONFINALDIR%"

echo Starting build
"%UNITYHUBROOT%\%UNITYVERSION%\Editor\Unity.exe" -batchmode -batchmodebuilder -projectPath %UNITYPROJECTPATH% -buildtarget %BUILDTARGET% -buildpath "%BUILDLOCATIONFINALDIR%\%PRODUCTNAME%.exe" -buildopts "CompressWithLz4HC" -logfile "%BUILDLOCATION%\Build-V%VERSION%.log"
if %ERRORLEVEL% NEQ 0 (
  echo Error durin build !!! 
  echo Please look at "%BUILDLOCATION%\Build-V%VERSION%.log" file
  exit /B 1
) else (
    echo End of build
)
GOTO:EOF