@echo off
SET LOGIN=John
SET GAMENAME=DOE
call:%~1
GOTO:EOF

:CheckConfig
if exist Sdk\butler (
  echo Itch.io sdk detected !
  echo Updating ...
  "%~dp0\..\Sdk\butler\butler.exe" update
  echo Checking config for project ...
  if not exist %CONFIGDIR%\itch\%CONFIGFILENAME% call:FirstConfig
  echo Config Ok !
)

GOTO:EOF

:FirstConfig - Function for creating first config
echo Itch.io sdk detected but no config found !
echo Creating one in %CONFIGDIR%\itch directory

if not exist %CONFIGDIR%\itch mkdir %CONFIGDIR%\itch

echo Enter informations from Itch.io
set /p USERNAME="Enter username: "
set /p GAMENAME="Enter gamename: "

@echo login:%USERNAME% >> "%CONFIGDIR%\itch\%CONFIGFILENAME%"
@echo gamename:%GAMENAME% >> "%CONFIGDIR%\itch\%CONFIGFILENAME%"

echo Executing first login for generating the key
"%~dp0\..\Sdk\butler\butler.exe" login -i %CONFIGDIR%\itch\%KEYFILENAME%

GOTO:EOF

:Upload  - Function for uploading to Ith.io
call:LoadConfig

echo Pushing to %LOGIN%/%GAMENAME%:%TOS%-%VERSION%%DEMOTAG% the %BUILDLOCATIONFINALDIR% on Itch.io
"%~dp0\..\Sdk\butler\butler.exe" -i %CONFIGDIR%\itch\%KEYFILENAME% push "%BUILDLOCATIONFINALDIR%" "%LOGIN%/%GAMENAME%:%TOS%%DEMOTAG%" --userversion %VERSION%
GOTO:EOF

:LoadConfig
echo Reading infos from itch config
for /f "tokens=1,2 delims=:" %%a in (%CONFIGDIR%\itch\%CONFIGFILENAME%) DO (
  if "%%a" == "login" SET LOGIN=%%b
  if "%%a" == "gamename" SET GAMENAME=%%b
)

SET LOGIN=%LOGIN:~0,-1%
SET GAMENAME=%GAMENAME:~0,-1%

GOTO:EOF