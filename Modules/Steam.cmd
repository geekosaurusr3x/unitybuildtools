@echo off
SET STEAMAPPID=0
SET STEAMDEPOTID=0
SET STEAMLOGIN=JONH
SET STEAMPASSWORD=AZERTY
call:%~1
GOTO:EOF

:CheckConfig
if exist Sdk\steambuilder (
  echo Steam SDK detected !
  echo Updating ...
  "%~dp0\..\Sdk\steambuilder\builder\steamcmd.exe" +quit
  echo Checking config for project ...
  if not exist %CONFIGDIR%\steam\%CONFIGFILENAME% call:FirstConfig
  echo Config Ok !
)
GOTO:EOF

:FirstConfig - Function for creating first config
echo Steam Sdk detected but no config found !
echo Creating one in %CONFIGDIR%\steam directory

if not exist %CONFIGDIR%\steam mkdir %CONFIGDIR%\steam

echo Enter informations from steam
set /p STEAMAPPID="Enter appid: "
set /p STEAMDEPOTID="Enter depotid: "
echo /!\Password will be stored in clear/!\
echo /!\Use only a bluid account/!\
set /p STEAMLOGIN="Enter account login: "
set /p STEAMPASSWORD="Enter accound password: "

@echo appid:%STEAMAPPID% >> "%CONFIGDIR%\steam\%CONFIGFILENAME%"
@echo depotid:%STEAMDEPOID% >> "%CONFIGDIR%\steam\%CONFIGFILENAME%"
@echo login:%STEAMLOGIN% >> "%CONFIGDIR%\steam\%CONFIGFILENAME%"
@echo password:%STEAMPASSWORD% >> "%CONFIGDIR%\steam\%CONFIGFILENAME%"

echo Executing first login for generating the key
"%~dp0\..\Sdk\steambuilder\builder\steamcmd.exe" +login "%STEAMLOGIN%" "%STEAMPASSWORD%"
GOTO:EOF

:Upload  - Function for uploading to SteamWorks
call:LoadConfig
call:ConfigureBuilDir

echo Pushing to appid %STEAMAPPID% depotoid %STEAMDEPOTID% the %BUILDLOCATIONFINALDIR% on Steam
"%~dp0\..\Sdk\steambuilder\builder\steamcmd.exe" +login "%STEAMLOGIN%" "%STEAMPASSWORD%" +run_app_build_http "%APPFILE%" +quit
GOTO:EOF

:LoadConfig
echo Reading infos from steam config
for /f "tokens=1,2 delims=:" %%a in (%CONFIGDIR%\steam\%CONFIGFILENAME%) DO (
  if "%%a" == "appid" SET STEAMAPPID=%%b
  if "%%a" == "depotid" SET STEAMDEPOTID=%%b
  if "%%a" == "login" SET STEAMLOGIN=%%b
  if "%%a" == "password" SET STEAMPASSWORD=%%b
)

SET STEAMAPPID=%STEAMAPPID:~0,-1%
SET STEAMDEPOTID=%STEAMDEPOTID:~0,-1%
SET STEAMLOGIN=%STEAMLOGIN:~0,-1%
SET STEAMPASSWORD=%STEAMPASSWORD:~0,-1%
set DEPOTFILE=%BUILDLOCATION%\steambuild\depot_build_%STEAMDEPOTID%.vdf
set APPFILE=%BUILDLOCATION%\steambuild\app_build_%STEAMAPPID%.vdf

GOTO:EOF

:ConfigureBuilDir - Function for creating first config

if exist "%BUILDLOCATION%\steambuild\" (
  echo Removing old depot config
  rmdir /S /Q "%BUILDLOCATION%\steambuild\"
)

echo Generating depot config
mkdir "%BUILDLOCATION%\steambuild\"
mkdir "%BUILDLOCATION%\output\"
echo f | @xcopy /y "%~dp0\..\Sdk\steambuilder\scripts\depot_build_1001.vdf" "%DEPOTFILE%" > NUL
echo f | @xcopy /y "%~dp0\..\Sdk\steambuilder\scripts\app_build_1000.vdf" "%APPFILE%" > NUL

::Update depot file
set INTEXTFILE=%DEPOTFILE%
set OUTTEXTFILE=%DEPOTFILE%_outs

set SEARCHTEXT="1001"
set REPLACETEXT="%STEAMDEPOTID%"
call:ReplaceInFile

set SEARCHTEXT="D:\MyGame\rel\master\"
set REPLACETEXT="%BUILDLOCATIONFINALDIR%"
call:ReplaceInFile

::Update App file
set INTEXTFILE=%APPFILE%
set OUTTEXTFILE=%APPFILE%_outs

set SEARCHTEXT="1000"
set REPLACETEXT="%STEAMAPPID%"
call:ReplaceInFile

set SEARCHTEXT="..\content\"
set REPLACETEXT="..\%VERSION%\"
call:ReplaceInFile

set SEARCHTEXT="1001"
set REPLACETEXT="%STEAMDEPOTID%"
call:ReplaceInFile

set SEARCHTEXT="depot_build_1001.vdf"
set REPLACETEXT="depot_build_%STEAMDEPOTID%.vdf"
call:ReplaceInFile

echo Depot config done

GOTO:EOF

:ReplaceInFile
setlocal enableDelayedExpansion
(
   for /F "tokens=1* delims=:" %%a in ('findstr /N "^" "%INTEXTFILE%"') do (
      set "line=%%b"
      if defined line set "line=!line:%SEARCHTEXT%=%REPLACETEXT%!"
      echo(!line!)
) > "%OUTTEXTFILE%"
endlocal

del "%INTEXTFILE%"
echo f | xcopy "%OUTTEXTFILE%" "%INTEXTFILE%" > NUL
del "%OUTTEXTFILE%"
endlocal
GOTO:EOF